# devops-netology

Следующие файлы исключены из индексирования git:  
- в каталоге /terraform:   
  - во вложенных папках /terraform;  
  - оканчивающиеся на .tfstate или в названии которых есть .tfstate.  
  - crash.log  
  - оканчивающиеся на .tfvars  
  - override.tf  
  - override.tf.json  
  - оканчивающиеся на _override.tf  
  - оканчивающиеся на _override.tf.json  
  - .terraformrc  
  - terraform.rc  
- каталог /.idea со всем содержимым